module.exports = {
  plugins: {
    'autoprefixer': {
      browsers: ['last 2 versions', '> 5%'],
    },
    'postcss-flexbugs-fixes': {},
  },
};
