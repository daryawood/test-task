const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const baseConfig = require('./base.config');

const projDirectory = process.cwd();

module.exports = merge(baseConfig, {
  devtool: 'eval-source-map',
  entry: [
    'babel-polyfill',
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://0.0.0.0:6060',
    'webpack/hot/only-dev-server',
    './src/app/index.js'
  ],
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.LoaderOptionsPlugin({
      debug: true,
      options: {
        context: projDirectory
      }
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development'
    }),
    new HtmlWebpackPlugin({
      template: './src/app/index.html',
      filename: 'index.html',
      production: false,
    }),
  ],
  devServer: {
    disableHostCheck: true,
    hot: true,
    port: 6060,
    host: '0.0.0.0',
    historyApiFallback: true,
    watchOptions: {
      poll: true
    },
    contentBase: './dist'
  }
});
