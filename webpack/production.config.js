const webpack = require('webpack');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const baseConfig = require('./base.config');

module.exports = merge(baseConfig, {
  devtool: 'source-map',
  entry: [
    'babel-polyfill',
    './src/app/index'
  ],
  output: {
    filename: '[name]-[chunkhash].min.js',
    chunkFilename: '[name]-[chunkhash].min.js',
    publicPath: '/'
  },
  plugins: [
    new CleanWebpackPlugin('dist', { root: process.cwd() }),
    new CompressionPlugin({
      asset: '[path].gz[query]',
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8
    }),
    new UglifyJSPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new HtmlWebpackPlugin({
      template: './src/app/index.html',
      filename: 'index.html'
    }),
    new webpack.LoaderOptionsPlugin({
      context: process.cwd(),
      minimize: true,
      debug: false,
    }),
    new ExtractTextPlugin({ filename: '[name]-[chunkhash].css', allChunks: true }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production'
    })
  ]
});
