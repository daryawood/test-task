const path = require('path');
const webpack = require('webpack');
const SvgStore = require('webpack-svgstore-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const projDirectory = process.cwd();
const curryAlias = aliasPath => path.join(projDirectory, aliasPath);

module.exports = {
  module: {
    rules: [
      { parser: { amd: false } },
      {
        test: /\.jsx?$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'postcss-loader', 'sass-loader']
        })
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file-loader?name=[name].[ext]'
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      }
    ]
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/'
  },
  resolve: {
    extensions: ['.jsx', '.js', '.json'],
    modules: [
      path.join(projDirectory, 'src', 'app'),
      'node_modules'
    ],
    alias: {
      app: curryAlias('src/app')
    }
  },
  plugins: [
    new webpack.LoaderOptionsPlugin({
      context: process.cwd()
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new SvgStore({
      svgoOptions: {
        plugins: [
          { removeTitle: true }
        ]
      },
      prefix: 'svg-'
    }),
    new CopyWebpackPlugin([
      {
        from: 'src/assets', to: 'assets'
      }
    ]),
    new ExtractTextPlugin('app.css')
  ]
};
