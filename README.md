# Тестовое задание на позицию frontend-разработчика в Semrush

## Требующиеся глобальные пакеты

- yarn или npm

## Установка зависимостей

`yarn` или `npm install`

## Локальный запуск с hot reload: http://0.0.0.0:6060/

`yarn dev` или `npm run dev`

## Продакшн-сборка

`yarn build` или `npm run build`

## Тесты

`yarn test` или `npm run build`

## Eslint

`yarn lint` или `npm run lint`

## Stylelint

`yarn lint-styles` или `npm run lint-styles`
