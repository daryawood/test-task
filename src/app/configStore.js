import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware } from 'react-router-redux';
import rootReducer from './reducers';
import DevTools from './components/DevTools';

export default (history, initialState = {}) => {
  let finalCreateStore;

  if (process.env.NODE_ENV === 'development') {
    finalCreateStore = compose(
      applyMiddleware(thunk),
      applyMiddleware(routerMiddleware(history)),
      DevTools.instrument()
    )(createStore);
  } else {
    finalCreateStore = compose(
      applyMiddleware(thunk),
      applyMiddleware(routerMiddleware(history))
    )(createStore);
  }

  const store = finalCreateStore(rootReducer, initialState);

  if (process.env.NODE_ENV === 'development' && module.hot) {
    module.hot.accept('reducers', () => {
      const nextRootReducer = require('reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};
