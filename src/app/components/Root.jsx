import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import _ from 'lodash';
import routes from 'app/routes';

function hashLinkScroll() {
  const { hash } = window.location;
  const hashSplitArr = hash.split('#');
  const resultHash = _.get(hashSplitArr, '[2]', null);

  if (resultHash !== null) {
    // Push onto callback queue so it runs after the DOM is updated,
    // this is required when navigating from a different page so that
    // the element is rendered on the page before trying to getElementById.
    setTimeout(() => {
      const element = document.getElementById(resultHash);
      if (element) element.scrollIntoView();
    }, 0);
  }
}

const Root = ({ store, history }) => (
  <Provider store={store}>
    <Router history={history} routes={routes} onUpdate={hashLinkScroll} />
  </Provider>
);

Root.propTypes = {
  store: PropTypes.shape({}).isRequired,
  history: PropTypes.shape({}).isRequired
};

export default Root;
