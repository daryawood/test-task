import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { deleteToast } from './actions';
import Toast from './Toast';

class ToastsStorage extends Component {
  constructor(props) {
    super(props);

    this.deleteToast = this.deleteToast.bind(this);
  }

  deleteToast(id) {
    return () => {
      this.props.deleteToast(id);
    };
  }

  render() {
    return (
      <div className="toast-wrap">
        {
          this.props.toasts.map(item =>
            (<Toast
              key={item.id}
              onClose={this.deleteToast(item.id)}
              message={item.message}
              type={item.toastType}
            />)
          )
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  toasts: state.toastsStorage.toasts
});

ToastsStorage.propTypes = {
  deleteToast: PropTypes.func.isRequired,
  toasts: PropTypes.arrayOf(PropTypes.shape({ // eslint-disable-line react/no-unused-prop-types
    id: PropTypes.string.isRequired,
    message: PropTypes.string.isRequired,
    toastType: PropTypes.string
  }))
};

export default connect(mapStateToProps, { deleteToast })(ToastsStorage);
