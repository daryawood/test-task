import React from 'react';
import renderer from 'react-test-renderer';
import { Provider } from 'react-redux';

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import ToastStorage from './../ToastStorage';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const store = mockStore({
  toastsStorage: {
    toasts: [
      {
        id: '1',
        message: 'First toast',
        toastType: 'error'
      },
      {
        id: '2',
        message: 'Second toast',
        toastType: 'warning'
      }
    ]
  }
});

test('Toasts storage renders correctly', () => {
  const tree = renderer.create(
    <Provider store={store}>
      <ToastStorage />
    </Provider>
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

