import {
  SHOW_TOAST,
  DELETE_TOAST
} from './../actions';
import reducer, { initialState } from './../reducers';

describe('toastsStorage reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual({
      ...initialState
    });
  });

  it('should handle SHOW_TOAST', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: SHOW_TOAST,
          payload: {
            id: 0,
            message: 'Error'
          }
        })
    ).toEqual({
      ...initialState,
      toasts: [{ id: 0, message: 'Error', toastType: 'error' }]
    });

    expect(
      reducer(
        {
          ...initialState,
          toasts: [{ id: 0, message: 'Error', toastType: 'error' }]
        },
        {
          type: SHOW_TOAST,
          payload: {
            id: 1,
            message: 'Error 1'
          }
        })
    ).toEqual({
      ...initialState,
      toasts: [{ id: 0, message: 'Error', toastType: 'error' }, { id: 1, message: 'Error 1', toastType: 'error' }]
    });
  });

  it('should handle DELETE_TOAST', () => {
    expect(
      reducer(
        {
          ...initialState,
          toasts: [{ id: 0, message: 'Error' }]
        },
        {
          type: DELETE_TOAST,
          payload: {
            id: 5
          }
        })
    ).toEqual({
      ...initialState,
      toasts: [{ id: 0, message: 'Error' }]
    });

    expect(
      reducer(
        {
          ...initialState,
          toasts: [{ id: 0, message: 'Error' }]
        },
        {
          type: DELETE_TOAST,
          payload: {
            id: 0
          }
        })
    ).toEqual({
      ...initialState,
      toasts: []
    });

    expect(
      reducer(
        {
          ...initialState,
          toasts: [{ id: 0, message: 'Error' }, { id: 1, message: 'Error 1' }]
        },
        {
          type: DELETE_TOAST,
          payload: {
            id: 1
          }
        })
    ).toEqual({
      ...initialState,
      toasts: [{ id: 0, message: 'Error' }]
    });
  });
});
