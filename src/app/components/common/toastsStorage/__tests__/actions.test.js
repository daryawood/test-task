import shortid from 'shortid';
import * as ToastsStorage from './../actions';

// mock shortid
shortid.generate = jest.fn(() => 123);

describe('toastsStorage actions', () => {
  it('should create an action show toast with default type of toast "error"', () => {
    const message = 'test';
    const toastType = 'error';

    const expectAction = {
      type: ToastsStorage.SHOW_TOAST,
      payload: {
        id: 123,
        message,
        toastType
      }
    };

    expect(ToastsStorage.showToast(message)).toEqual(expectAction);
  });

  it('should create an action show toast with default type of toast "error"', () => {
    const message = 'test';
    const toastType = 'success';

    const expectAction = {
      type: ToastsStorage.SHOW_TOAST,
      payload: {
        id: 123,
        message,
        toastType
      }
    };

    expect(ToastsStorage.showToast(message, toastType)).toEqual(expectAction);
  });

  it('should create an action delete error', () => {
    const id = 1;

    const expectAction = {
      type: ToastsStorage.DELETE_TOAST,
      payload: {
        id
      }
    };

    expect(ToastsStorage.deleteToast(id)).toEqual(expectAction);
  });
});
