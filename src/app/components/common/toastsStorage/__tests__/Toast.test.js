import React from 'react';
import renderer from 'react-test-renderer';
import Toast from './../Toast';

test('Simple toast renders correctly', () => {
  const props = {
    message: 'My message',
    onClose: () => {}
  };

  const tree = renderer.create(
    <Toast {...props} />
  ).toJSON();

  expect(tree).toMatchSnapshot();
});

test('Toast with all available props renders correctly', () => {
  const props = {
    message: 'My message',
    duration: 10000,
    onClose: () => {},
    type: 'warning'
  };

  const tree = renderer.create(
    <Toast {...props} />
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
