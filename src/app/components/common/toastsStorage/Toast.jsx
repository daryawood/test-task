import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/common/Icon/Icon';

class Toast extends Component {
  constructor(props) {
    super(props);

    this.close = this.close.bind(this);
    this.clearCloseTimer = this.clearCloseTimer.bind(this);
  }

  componentDidMount() {
    if (this.props.duration) {
      this.closeTimer = setTimeout(() => {
        this.close();
      }, this.props.duration);
    }
  }

  componentWillUnmount() {
    this.clearCloseTimer();
  }

  close() {
    this.clearCloseTimer();
    this.props.onClose();
  }

  clearCloseTimer() {
    if (this.closeTimer) {
      clearTimeout(this.closeTimer);
      this.closeTimer = null;
    }
  }

  render() {
    return (
      <div className={`toast toast_type_${this.props.type}`}>
        <button className="toast__close" onClick={this.close}>
          <Icon
            name="close"
          />
        </button>
        <div className="toast__text">
          {this.props.message}
        </div>
      </div>
    );
  }
}

Toast.defaultProps = {
  duration: 15000,
  type: 'error'
};

Toast.propTypes = {
  duration: PropTypes.number,
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string.isRequired,
  type: PropTypes.string
};

export default Toast;
