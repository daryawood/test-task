import shortid from 'shortid';

export const SHOW_TOAST = 'SHOW_TOAST';
export const DELETE_TOAST = 'DELETE_TOAST';

export const showToast = (message, type) => {
  const toastType = type || 'error';

  return {
    type: SHOW_TOAST,
    payload: {
      id: shortid.generate(),
      message,
      toastType
    }
  };
};

export const deleteToast = id => ({
  type: DELETE_TOAST,
  payload: {
    id
  }
});
