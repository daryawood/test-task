import {
  SHOW_TOAST,
  DELETE_TOAST
} from './actions';

export const initialState = {
  toasts: []
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SHOW_TOAST:
      return {
        ...state,
        toasts: [
          ...state.toasts,
          {
            id: action.payload.id,
            message: action.payload.message,
            toastType: action.payload.toastType || 'error'
          }
        ]
      };
    case DELETE_TOAST:
      return {
        ...state,
        toasts: state.toasts.filter(toast => toast.id !== action.payload.id)
      };
    default:
      return state;
  }
}
