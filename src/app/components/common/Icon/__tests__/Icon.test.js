import React from 'react';
import renderer from 'react-test-renderer';
import Icon from './../Icon';

test('Icon renders correctly', () => {
  const props = {
    name: 'my-svg',
    className: 'my-class'
  };

  const tree = renderer.create(
    <Icon {...props} />
  ).toJSON();

  expect(tree).toMatchSnapshot();
});
