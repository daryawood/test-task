import React from 'react';
import PropTypes from 'prop-types';

const Icon = props => (
  <svg className={`${props.className} b-svg__${props.name}`} role="img">
    <use xlinkHref={`#svg-${props.name}`} />
  </svg>
);

Icon.defaultProps = {
  className: ''
};

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string
};

export default Icon;
