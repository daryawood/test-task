import React from 'react';
import Icon from 'app/components/common/Icon/Icon';
import { Link } from 'react-router';

const Header = () => (
  <header className="b-header">
    <div className="container b-header__inner">
      <Link to="/">
        <Icon
          name="semrush-logo"
          className="b-header__logo"
        />
      </Link>
    </div>
  </header>
);

export default Header;
