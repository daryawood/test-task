import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ToastsStorage from 'components/common/toastsStorage/ToastStorage';
import DevTools from './../DevTools';
import Header from './Header';
import { getPosts } from './../../routes/home/actions';

class Layout extends Component {
  componentDidMount() {
    this.props.getPosts();
  }

  render() {
    return (
      <div className="b-layout">
        <ToastsStorage
          errors={this.props.toasts}
        />
        <Header />
        <main className="b-main">
          {process.env.NODE_ENV !== 'production' && <DevTools />}
          {this.props.children}
        </main>
      </div>
    );
  }
}

Layout.propTypes = {
  children: PropTypes.element.isRequired,
  getPosts: PropTypes.func,
  toasts: PropTypes.arrayOf(PropTypes.shape())
};

const mapStateToProps = (state, ownProps) => ({
  location: ownProps.location,
  params: ownProps.params,
  toasts: state.toastsStorage.toasts
});

export default connect(mapStateToProps, { getPosts })(Layout);
