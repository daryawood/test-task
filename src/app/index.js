import React from 'react';
import ReactDom from 'react-dom';
import { hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { AppContainer } from 'react-hot-loader';
import configStore from './configStore';
import Root from './components/Root';

const __svg__ = { path: './../assets/svg/**/*.svg', name: 'logos.svg' };

require('webpack-svgstore-plugin/src/helpers/svgxhr')(__svg__);
require('./../styles/styles.scss');

const store = configStore();
const history = syncHistoryWithStore(hashHistory, store);
const rootEl = document.getElementById('root');

const render = (Component) => {
  ReactDom.render(
    <AppContainer>
      <Component store={store} history={history} />
    </AppContainer>,
    rootEl
  );
};
render(Root);

if (module.hot) {
  module.hot.accept('./components/Root', () => {
    render(Root);
  });
}
