import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import toastsStorage from 'components/common/toastsStorage/reducers';
import posts from './routes/home/reducers';

export default combineReducers({
  routing,
  posts,
  toastsStorage
});
