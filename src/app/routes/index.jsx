import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Layout from '../components/Layout/Layout';
import Home from './home/Home';
import Post from './post/Post';

const onChange = (prevState, nextState) => {
  if (nextState.location.pathname !== prevState.location.pathname) {
    window.scrollTo(0, 0);
  }
};

export default (
  <div>
    <Route
      exact
      path="/"
      component={Layout}
      onChange={onChange}
    >

      <IndexRoute component={Home} />

      <Route
        component={Post}
        path="/posts/:id"
      />

    </Route>
  </div>
);
