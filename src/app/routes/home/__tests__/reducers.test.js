import reducer, { initialState } from './../reducers';

import {
  ADD_NEW_POST,
  GET_POSTS,
  GET_SINGLE_POST,
  DELETE_POST,
  UPDATE_POST
} from './../actions';

// mock Date
Date.now = jest.fn();

describe('progress reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual({
      ...initialState
    });
  });

  it('should handle GET_POSTS', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: GET_POSTS,
          payload: { 1: {}, 2: {} }
        })
    ).toEqual({
      ...initialState,
      results: { 1: {}, 2: {} },
    });
  });

  it('should handle GET_SINGLE_POST', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: GET_SINGLE_POST,
          payload: { someProp: 'value' }
        })
    ).toEqual({
      ...initialState,
      currentPost: { someProp: 'value' },
    });
  });

  it('should handle ADD_NEW_POST', () => {
    expect(
      reducer(
        {
          ...initialState
        },
        {
          type: ADD_NEW_POST,
          payload: { 123: { someProp: 'value' } }
        })
    ).toEqual({
      ...initialState,
      results: { 123: { someProp: 'value' } },
    });
  });

  it('should handle ADD_NEW_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {
            54: {
              someProp: 'value'
            }
          }
        },
        {
          type: ADD_NEW_POST,
          payload: { 123: { someProp: 'value' } }
        })
    ).toEqual({
      ...initialState,
      results: { 54: { someProp: 'value' }, 123: { someProp: 'value' } },
    });
  });

  it('should handle DELETE_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {
            54: {
              someProp: 'value'
            }
          }
        },
        {
          type: DELETE_POST,
          payload: 54
        })
    ).toEqual({
      ...initialState,
      results: {},
    });
  });

  it('should handle DELETE_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {
            21: {
              someProp: 'value'
            },
            54: {
              someProp: 'value'
            }
          }
        },
        {
          type: DELETE_POST,
          payload: 54
        })
    ).toEqual({
      ...initialState,
      results: { 21: { someProp: 'value' } },
    });
  });

  it('should handle DELETE_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {
            54: {
              someProp: 'value'
            }
          }
        },
        {
          type: DELETE_POST,
          payload: 21
        })
    ).toEqual({
      ...initialState,
      results: { 54: { someProp: 'value' } },
    });
  });

  it('should handle DELETE_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {}
        },
        {
          type: DELETE_POST,
          payload: 21
        })
    ).toEqual({
      ...initialState,
      results: {},
    });
  });

  it('should handle UPDATE_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {
            54: {
              someProp: 'value'
            }
          }
        },
        {
          type: UPDATE_POST,
          payload: {
            id: 54,
            data: {
              newProp: 'newValue'
            }
          }
        })
    ).toEqual({
      ...initialState,
      results: {
        54: {
          someProp: 'value',
          newProp: 'newValue',
          creationDate: Date.now()
        }
      },
    });
  });

  it('should handle UPDATE_POST', () => {
    expect(
      reducer(
        {
          ...initialState,
          results: {
            54: {
              someProp: 'value',
              modifiedProp: 'old value'
            }
          }
        },
        {
          type: UPDATE_POST,
          payload: {
            id: 54,
            data: {
              newProp: 'newValue',
              modifiedProp: 'new value'
            }
          }
        })
    ).toEqual({
      ...initialState,
      results: {
        54: {
          someProp: 'value',
          newProp: 'newValue',
          modifiedProp: 'new value',
          creationDate: Date.now()
        }
      },
    });
  });
});
