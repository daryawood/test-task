import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import posts from 'app/posts.json';
import shortid from 'shortid';
import { SHOW_TOAST } from 'components/common/toastsStorage/actions';
import * as Posts from './../actions';

const { GET_POSTS, GET_SINGLE_POST, ADD_NEW_POST, DELETE_POST, UPDATE_POST } = Posts;

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

// mock shortid
shortid.generate = jest.fn(() => 123);

describe('posts actions', () => {
  it('should create an action postsSuccess', () => {
    const expectAction = {
      type: GET_POSTS,
      payload: { 1: {}, 2: {} }
    };

    expect(Posts.postsSuccess({ 1: {}, 2: {} })).toEqual(expectAction);
  });

  it('should create an action getPosts', () => {
    const expectedActions = [{
      type: GET_POSTS,
      payload: posts
    }];

    const store = mockStore({ });

    store.dispatch(Posts.getPosts());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should create an action getPostById', () => {
    const expectedActions = [{
      type: GET_SINGLE_POST,
      payload: posts[1]
    }];

    const store = mockStore({ });

    store.dispatch(Posts.getPostById(1));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should create an action addNewPost', () => {
    const expectedActions = [{
      type: ADD_NEW_POST,
      payload: {
        newPost1: {
          id: 'newPost1',
          ...Posts.draftPost
        }
      }
    }];

    const store = mockStore({ });

    store.dispatch(Posts.addNewPost('newPost1'));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should create an action deletePost', () => {
    const expectAction = {
      type: DELETE_POST,
      payload: 221
    };

    expect(Posts.deletePost(221)).toEqual(expectAction);
  });

  it('should create an action updatePost', () => {
    const expectAction = {
      type: UPDATE_POST,
      payload: {
        id: 221,
        data: {}
      }
    };

    expect(Posts.updatePost(221, {})).toEqual(expectAction);
  });

  it('should create an action addSinglePost', () => {
    const expectedActions = [{
      type: ADD_NEW_POST,
      payload: {
        123: {
          id: 123,
          ...Posts.draftPost
        }
      }
    }];

    const store = mockStore({ posts: { results: posts } });

    store.dispatch(Posts.addSinglePost());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should create an action deleteSinglePost', () => {
    const expectedActions = [{
      type: DELETE_POST,
      payload: 1
    }];

    const store = mockStore({ posts: { results: posts } });

    store.dispatch(Posts.deleteSinglePost(1));
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should create an action updateSinglePost', () => {
    const expectedActions = [
      {
        type: UPDATE_POST,
        payload: {
          id: 1,
          data: {}
        }
      },
      {
        type: SHOW_TOAST,
        payload: {
          id: 123,
          message: 'Your post has been saved successfully!',
          toastType: 'success'
        }
      }
    ];

    const store = mockStore({ posts: { results: posts } });

    store.dispatch(Posts.updateSinglePost(1, {}));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
