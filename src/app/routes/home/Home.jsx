import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import _ from 'lodash';
import { addSinglePost } from './actions';
import Search from './components/search/Search';
import Article from './components/article/Article';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: props.posts.results,
      searchStr: ''
    };

    this.handleSearch = this.handleSearch.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.posts.results !== this.state.posts.results) {
      this.setState({
        posts: nextProps.posts.results
      });
    }
  }

  handleSearch(searchStr) {
    let posts;

    if (searchStr === '') {
      posts = this.props.posts.results;
    } else {
      const filteredResults = {};
      _.forEach(this.state.posts, (item) => {
        if (item.title.toLowerCase().indexOf(searchStr.toLowerCase()) !== -1) {
          filteredResults[item.id] = item;
        }
      });

      posts = filteredResults;
    }

    this.setState({
      posts,
      searchStr
    });
  }

  render() {
    const { posts, searchStr } = this.state;
    let result = null;

    if (_.isEmpty(posts)) {
      result = <div className="b-home__claim">There&#39;re not any posts: change your search or add new post</div>;
    } else {
      result = Object.keys(posts).map(
        id => <Article key={shortid.generate()} className="b-home__article" id={id} article={posts[id]} />
      );
    }

    return (
      <section className="b-home">
        <div className="b-home__top">
          <div className="container">
            <div className="b-home__header">
              <h1 className="b-home__heading">My posts</h1>
              <button
                className="b-home__btn b-btn"
                onClick={this.props.addSinglePost}
              >New post</button>
            </div>
            <Search
              className="b-home__search"
              placeholder="Search"
              handleSubmit={this.handleSearch}
              value={searchStr}
            />
          </div>
        </div>
        <div className="b-shadow b-home__shadow">
          <div className="b-shadow__inner" />
        </div>
        <div className="b-home__bottom container">
          <div className="row b-home__articles">
            {result}
          </div>
        </div>
      </section>
    );
  }
}


const mapStateToProps = state => ({
  posts: state.posts
});

Home.propTypes = {
  posts: PropTypes.shape({
    results: PropTypes.shape({})
  }),
  addSinglePost: PropTypes.func
};

export default connect(mapStateToProps, { addSinglePost })(Home);
