import posts from 'app/posts.json';
import shortid from 'shortid';
import { hashHistory } from 'react-router';
import store from 'store';
import { showToast } from 'components/common/toastsStorage/actions';

export const GET_POSTS = 'GET_POSTS';
export const GET_SINGLE_POST = 'GET_SINGLE_POST';
export const ADD_NEW_POST = 'ADD_NEW_POST';
export const DELETE_POST = 'DELETE_POST';
export const UPDATE_POST = 'UPDATE_POST';

export const draftPost = {
  title: '',
  image: '',
  author: 'Hellen Wilson',
  authorImage: 'assets/images/avatar.jpg',
  status: 'draft',
  statusDetails: '',
  creationDate: Date.now(),
  publicationDate: '',
  category: '',
  views: 0,
  rating: 0,
  shares: 0,
  content: '',
  teaser: '',
  socialImage: {
    src: '',
    title: false,
    logo: false,
    autoMode: true
  },
  seo: {
    title: '',
    description: '',
    keywords: ''
  }
};

const postsFromLocalStorage = store.get('posts');

const updateStore = (data) => {
  store.set('posts', data);
};

export const postsSuccess = data => ({
  type: GET_POSTS,
  payload: data
});

export const getPosts = () => (dispatch) => {
  if (postsFromLocalStorage) {
    dispatch(postsSuccess(postsFromLocalStorage));
  } else {
    dispatch(postsSuccess(posts));
    updateStore(posts);
  }
};

export const getPostById = (id) => {
  let payload;

  if (postsFromLocalStorage) {
    payload = postsFromLocalStorage[id];
  } else {
    payload = posts[id];
  }

  return {
    type: GET_SINGLE_POST,
    payload
  };
};

export const addNewPost = id => ({
  type: ADD_NEW_POST,
  payload: {
    [id]: {
      id,
      ...draftPost
    }
  }
});

export const deletePost = id => ({
  type: DELETE_POST,
  payload: id
});

export const updatePost = (id, data) => ({
  type: UPDATE_POST,
  payload: {
    id,
    data
  }
});

export const addSinglePost = () => (dispatch, getState) => {
  const id = shortid.generate();
  dispatch(addNewPost(id));
  updateStore(getState().posts.results);
  hashHistory.push(`/posts/${id}`);
};

export const deleteSinglePost = id => (dispatch, getState) => {
  dispatch(deletePost(id));
  updateStore(getState().posts.results);
  hashHistory.push('/');
};

export const updateSinglePost = (id, data) => (dispatch, getState) => {
  dispatch(updatePost(id, data));
  updateStore(getState().posts.results);
  dispatch(showToast('Your post has been saved successfully!', 'success'));
};
