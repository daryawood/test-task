import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'app/components/common/Icon/Icon';

class Search extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.value
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.clearSearch = this.clearSearch.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value !== this.state.value) {
      this.setState({
        value: nextProps.value
      });
    }
  }

  handleInputChange(e) {
    const value = e.target.value;

    this.setState({
      value
    }, this.handleSearch);
  }

  clearSearch() {
    this.setState({
      value: ''
    }, this.handleSearch);
  }

  handleSearch() {
    const { value } = this.state;

    this.props.handleSubmit(value);
  }

  render() {
    const { className, placeholder } = this.props;
    const { value } = this.state;

    return (
      <div className={`b-search ${className}`}>
        <input
          type="text"
          className="b-search__input"
          value={value}
          placeholder={placeholder}
          onChange={this.handleInputChange}
        />
        <button
          className="b-search__btn b-search__btn_type_glass"
          onClick={this.handleSearch}
        />
        <button
          className={`b-search__btn b-search__btn_type_close ${value.length === 0 ? 'is-hidden' : ''}`}
          onClick={this.clearSearch}
        >
          <Icon className="b-search__close-icon" name="close" />
        </button>
      </div>
    );
  }
}

Search.defaultProps = {
  placeholder: '',
  value: '',
  className: ''
};

Search.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

export default Search;
