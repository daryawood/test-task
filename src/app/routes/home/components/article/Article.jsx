import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';
import Icon from 'components/common/Icon/Icon';

const Article = (props) => {
  const { article, className, id } = props;

  const handleInfoClick = (e) => {
    e.preventDefault();
  };

  const publicationDateOpts = {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  };

  const creationDateOpts = {
    ...publicationDateOpts,
    hour: 'numeric',
    minute: 'numeric',
    hour12: false
  };

  let articleImage = null;

  if (article.image) {
    articleImage = (<div className="b-article__image-wrap">
      <img
        className="b-article__image"
        src={article.image}
        alt={article.title}
      />
    </div>);
  } else {
    articleImage = <Icon name="fireball" className="b-article__logo" />;
  }

  return (
    <Link to={`/posts/${id}`} className={`b-article ${className}`}>
      <div className={`b-article__top ${!article.image ? 'b-article__top_type_cover' : ''}`}>
        {articleImage}
        <div
          className={`b-article__label b-label b-label_type_${article.status.split(' ').join('-')}`}
        >
          <div className="b-label__text">{article.status}</div>
          {
            article.statusDetails &&
              <div className="b-label__info">
                <button
                  className="b-label__icon-wrap"
                  onClick={handleInfoClick}
                >
                  <Icon
                    className="b-label__icon"
                    name="info"
                  />
                </button>
                <div className="b-label__tooltip">
                  {article.statusDetails}
                </div>
              </div>
          }
        </div>
      </div>
      <div className="b-article__content">
        <div className="b-article__info">
          <div className="b-article__date">
            {
              article.status === 'published' ?
                new Date(Number(article.publicationDate)).toLocaleString('en-US', publicationDateOpts)
                :
                `Updated: ${new Date(Number(article.creationDate)).toLocaleString('en-US', creationDateOpts)}`
            }
          </div>
          {
            article.category && <div className="b-article__category">{article.category}</div>
          }
        </div>
        <h2 className="b-article__heading">{article.title}</h2>
        {
          article.status === 'published' &&
            <div className="b-article__social b-social">
              <div className="b-social__item">
                <Icon
                  className="b-social__icon b-social__icon_type_eye"
                  name="eye"
                />
                <div className="b-social__num">
                  {article.views}
                </div>
              </div>
              <div className="b-social__item">
                <Icon
                  className="b-social__icon b-social__icon_type_star"
                  name="star"
                />
                <div className="b-social__num">
                  {article.rating}
                </div>
              </div>
              <div className="b-social__item">
                <Icon
                  className="b-social__icon b-social__icon_type_share"
                  name="share"
                />
                <div className="b-social__num">
                  {article.shares}
                </div>
              </div>
            </div>
        }
      </div>
    </Link>
  );
};

Article.defaultProps = {
  className: ''
};

Article.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  article: PropTypes.objectOf({
    id: PropTypes.string.isRequired,
    image: PropTypes.string,
    title: PropTypes.string,
    status: PropTypes.string.isRequired,
    statusDetails: PropTypes.string,
    category: PropTypes.string,
    publicationDate: PropTypes.string,
    creationDate: PropTypes.string,
    views: PropTypes.string,
    rating: PropTypes.string,
    shares: PropTypes.string,
  }).isRequired
};

export default Article;
