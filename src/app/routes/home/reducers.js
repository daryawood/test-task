import _ from 'lodash';
import {
  ADD_NEW_POST,
  GET_POSTS,
  GET_SINGLE_POST,
  DELETE_POST,
  UPDATE_POST
} from './actions';

export const initialState = {
  results: {},
  currentPost: {}
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        results: action.payload
      };
    case GET_SINGLE_POST:
      return {
        ...state,
        currentPost: action.payload
      };
    case ADD_NEW_POST:
      return {
        ...state,
        results: {
          ...state.results,
          ...action.payload
        }
      };
    case DELETE_POST:
      return {
        ...state,
        results: _.omit(state.results, action.payload)
      };
    case UPDATE_POST: {
      return {
        ...state,
        results: {
          ...state.results,
          [action.payload.id]: {
            ...state.results[action.payload.id],
            ...action.payload.data,
            creationDate: Date.now()
          }
        }
      };
    }
    default:
      return state;
  }
}
