import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _ from 'lodash';
import Icon from 'components/common/Icon/Icon';
import { getPostById, deleteSinglePost, updateSinglePost } from './../home/actions';
import ImageUploader from './components/imageUploader/ImageUploader';
import Teaser from './components/teaser/Teaser';
import SeoForm from './components/seoForm/SeoForm';
import SocialImage from './components/socialImage/SocialImage';
import Editor from './components/editor/Editor';
import Title from './components/title/Title';

class Post extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      image: '',
      teaser: '',
      seo: {
        title: '',
        description: '',
        keywords: ''
      },
      socialImage: {
        src: '',
        title: false,
        logo: false,
        autoMode: true
      },
      content: ''
    };

    this.handleSeoChanges = this.handleSeoChanges.bind(this);
    this.handleTeaserChanges = this.handleTeaserChanges.bind(this);
    this.handleMainImageChange = this.handleMainImageChange.bind(this);
    this.handleSocialImageChange = this.handleSocialImageChange.bind(this);
    this.handleContentChange = this.handleContentChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.deletePost = this.deletePost.bind(this);
    this.updatePost = this.updatePost.bind(this);
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.props.getPostById(this.props.params.id);
    }
  }

  componentWillReceiveProps(nextProps) {
    const { params: { id } } = this.props;

    if (id !== nextProps.params.id) {
      this.props.getPostById(nextProps.params.id);
    }

    this.setState({
      teaser: nextProps.currentPost.teaser,
      title: nextProps.currentPost.title,
      seo: nextProps.currentPost.seo,
      image: nextProps.currentPost.image,
      socialImage: nextProps.currentPost.socialImage,
      content: nextProps.currentPost.content
    });
  }

  handleSeoChanges(seo) {
    this.setState({
      seo: {
        ...this.state.seo,
        ...seo
      }
    });
  }

  handleTeaserChanges(teaser) {
    this.setState({
      teaser
    });
  }

  handleMainImageChange(image) {
    this.setState({
      image
    });
  }

  handleSocialImageChange(socialImage) {
    this.setState({
      socialImage: {
        ...this.state.socialImage,
        ...socialImage
      }
    });
  }

  handleContentChange(content) {
    this.setState({
      content
    });
  }

  handleTitleChange(title) {
    this.setState({
      title
    });
  }

  deletePost() {
    this.props.deleteSinglePost(this.props.params.id);
  }

  updatePost() {
    this.props.updateSinglePost(this.props.params.id, this.state);
  }

  render() {
    const { currentPost } = this.props;
    const { teaser, seo, image, socialImage, title, content } = this.state;

    if (_.isEmpty(currentPost)) {
      return null;
    }

    return (
      <div className="b-post">
        <div className="container">
          <div className="b-post__header">
            <Link className="b-post__return-btn" to="/">
              <Icon name="arrow-left" className="b-post__return-icon" />
            </Link>
            <Title
              className="b-post__heading"
              title={title}
              onChange={this.handleTitleChange}
            />
          </div>
          <div className="b-post__info">
            <div className="b-post__author b-avatar">
              <img src={currentPost.authorImage} alt={currentPost.author} className="b-avatar__img" />
              <div className="b-avatar__name">{currentPost.author}</div>
            </div>
            <div className={`b-post__label b-label b-label_type_${currentPost.status.split(' ').join('-')}`}>
              <div className="b-label__text">{currentPost.status}</div>
            </div>
          </div>
          <ImageUploader
            imagePreviewUrl={image}
            className="b-post__image-uploader"
            onChange={this.handleMainImageChange}
          />
        </div>
        <div className="b-post__md-editor b-editor">
          <div className="b-editor__bg">
            <div className="b-shadow b-editor__shadow">
              <div className="b-shadow__inner" />
            </div>
          </div>
          <div className="container">
            <Editor
              content={content}
              onChange={this.handleContentChange}
            />
          </div>
        </div>
        <div className="b-shadow b-post__shadow">
          <div className="b-shadow__inner" />
        </div>
        <div className="container">
          <Teaser
            className="b-post__teaser"
            teaser={teaser}
            onChange={this.handleTeaserChanges}
          />
          <div className="b-post__seo">
            <h2 className="b-post__h2">SEO information</h2>
            <div className="b-post__prompt">Here you can specify information that will help search engines discover your post and display it in search results. It is also used when sharing a post on social media. If you&#39;re unsure, leave the fields empty.</div>
            <div className="b-post__uploader-title b-small-title">Image to be displayed on social media</div>
            <SocialImage
              className="b-post__social-image"
              defaultImage={currentPost.image}
              content={title}
              title={socialImage.title}
              logo={socialImage.logo}
              image={socialImage.src}
              autoMode={socialImage.autoMode}
              onChange={this.handleSocialImageChange}
            />
            <SeoForm
              className="b-post__seo-form"
              title={seo.title}
              description={seo.description}
              keywords={seo.keywords}
              onChange={this.handleSeoChanges}
            />
          </div>
        </div>
        <div className="b-post__footer">
          <div className="container b-post__footer-inner">
            <button className="b-post__delete b-icon-btn" onClick={this.deletePost}>
              <Icon name="trash" className="b-icon-btn__icon b-post__delete-icon" />
              <span className="b-icon-btn__text">Delete</span>
            </button>
            <button className="b-btn b-btn_theme_green" onClick={this.updatePost}>Save</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentPost: state.posts.currentPost
});

Post.propTypes = {
  currentPost: PropTypes.shape({
    title: PropTypes.string,
    author: PropTypes.string,
    authorImage: PropTypes.string,
    status: PropTypes.string,
    teaser: PropTypes.string,
    image: PropTypes.string,
    socialImage: PropTypes.shape({
      src: PropTypes.string,
      logo: PropTypes.bool,
      title: PropTypes.bool,
      isAuto: PropTypes.bool,
    }),
    seo: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      keywords: PropTypes.string,
    }),
    content: PropTypes.string
  }),
  getPostById: PropTypes.func.isRequired,
  deleteSinglePost: PropTypes.func.isRequired,
  updateSinglePost: PropTypes.func.isRequired,
  params: PropTypes.shape({
    id: PropTypes.string
  }),
};

export default connect(mapStateToProps, { getPostById, deleteSinglePost, updateSinglePost })(Post);
