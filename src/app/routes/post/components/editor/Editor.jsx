import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

class Editor extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value) {
    this.props.onChange(value);
  }

  render() {
    return (
      <ReactQuill
        value={this.props.content}
        onChange={this.handleChange}
      />
    );
  }
}

Editor.propTypes = {
  onChange: PropTypes.func.isRequired,
  content: PropTypes.string,
};

export default Editor;
