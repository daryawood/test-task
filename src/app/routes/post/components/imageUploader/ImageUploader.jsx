import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/common/Icon/Icon';

class ImageUploader extends Component {
  constructor(props) {
    super(props);

    this.handleImageUpload = this.handleImageUpload.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
  }

  handleImageUpload(e) {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.props.onChange(reader.result);
    };

    reader.readAsDataURL(file);
  }

  deleteImage() {
    this.props.onChange('');
  }

  render() {
    const { className, imagePreviewUrl } = this.props;

    let imageUploaderContent = null;

    if (imagePreviewUrl.length === 0) {
      imageUploaderContent =
        (<div className="b-image-uploader__inner">
          <input
            type="file"
            onChange={this.handleImageUpload}
            className="b-image-uploader__input"
          />
          <div className="b-image-uploader__icon-wrap">
            <Icon className="b-image-uploader__upload-icon" name="upload" />
          </div>
          <div className="b-image-uploader__heading">Featured image</div>
          <div className="b-image-uploader__action">select an image file to upload or drag it here</div>
          <div className="b-image-uploader__text">Please see the requirements for our Featured image. If you are unsure what image you should select, do not upload anything. Our editor will upload an appropriate image when reviewing your post.</div>
        </div>);
    } else {
      imageUploaderContent =
        (<div className="b-image-uploader__img-wrap">
          <img src={imagePreviewUrl} alt="Preview" className="b-image-uploader__img" />
          <button className="b-image-uploader__trash" onClick={this.deleteImage}>
            <Icon name="trash" className="b-image-uploader__trash-icon" />
          </button>
          <div className="b-image-uploader__area">
            <div className="b-image-uploader__warning">This area will not be displayed on the post page</div>
          </div>
        </div>);
    }

    return (
      <div className={`b-image-uploader ${className}`}>
        {imageUploaderContent}
      </div>
    );
  }
}

ImageUploader.propTypes = {
  className: PropTypes.string,
  imagePreviewUrl: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

ImageUploader.defaultProps = {
  className: '',
  imagePreviewUrl: '',
};

export default ImageUploader;
