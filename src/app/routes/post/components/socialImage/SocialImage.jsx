import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Uploader from './components/Uploader';
import AutoPreview from './components/AutoPreview';

class SocialImage extends Component {
  constructor(props) {
    super(props);

    this.handleImageUpload = this.handleImageUpload.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
    this.toggleLogo = this.toggleLogo.bind(this);
    this.toggleTitle = this.toggleTitle.bind(this);
    this.switchOnAutoMode = this.switchOnAutoMode.bind(this);
    this.switchOffAutoMode = this.switchOffAutoMode.bind(this);
  }

  handleImageUpload(e) {
    e.preventDefault();

    const reader = new FileReader();
    const file = e.target.files[0];

    reader.onloadend = () => {
      this.props.onChange({
        src: reader.result
      });
    };

    reader.readAsDataURL(file);
  }

  deleteImage() {
    this.props.onChange({
      src: ''
    });
  }

  toggleLogo() {
    this.props.onChange({
      logo: !this.props.logo
    });
  }

  toggleTitle() {
    this.props.onChange({
      title: !this.props.title
    });
  }

  switchOnAutoMode() {
    this.props.onChange({
      autoMode: true
    });
  }

  switchOffAutoMode() {
    this.props.onChange({
      autoMode: false
    });
  }

  render() {
    const { className, content, image, autoMode, title, logo, defaultImage } = this.props;

    let innerSocialImage = null;

    if (autoMode) {
      innerSocialImage =
        (<AutoPreview
          content={content}
          title={title}
          logo={logo}
          defaultImage={defaultImage}
        />);
    } else {
      innerSocialImage =
        (<Uploader
          image={image}
          deleteImage={this.deleteImage}
          handleImageUpload={this.handleImageUpload}
        />);
    }

    const checkboxes = (
      <div className="b-social-image__checkboxes">
        <div className="b-social-image__checkbox-row">
          <input
            className="b-social-image__checkbox"
            id="isTitleNeeded"
            type="checkbox"
            name="isTitleNeeded"
            onChange={this.toggleTitle}
            checked={title}
          />
          <label className="b-social-image__label" htmlFor="isTitleNeeded">
              Add title
          </label>
        </div>
        <div className="b-social-image__checkbox-row">
          <input
            className="b-social-image__checkbox"
            id="isLogoNeeded"
            type="checkbox"
            name="isLogoNeeded"
            onChange={this.toggleLogo}
            checked={logo}
          />
          <label className="b-social-image__label" htmlFor="isLogoNeeded">
              Add logo
          </label>
        </div>
      </div>
    );

    return (
      <div className={`b-social-image ${className}`}>
        <div className="b-social-image__btns">
          <button
            className={`b-social-image__btn ${autoMode ? 'b-social-image__btn_type_active' : ''}`}
            onClick={this.switchOnAutoMode}
          >
            Create automatically
          </button>
          <button
            className={`b-social-image__btn ${!autoMode ? 'b-social-image__btn_type_active' : ''}`}
            onClick={this.switchOffAutoMode}
          >
            Upload manually
          </button>
        </div>
        { autoMode && checkboxes }
        <div className="b-social-image__inner">
          {innerSocialImage}
        </div>
      </div>
    );
  }
}

SocialImage.propTypes = {
  className: PropTypes.string,
  image: PropTypes.string,
  defaultImage: PropTypes.string,
  content: PropTypes.string,
  autoMode: PropTypes.bool,
  title: PropTypes.bool,
  logo: PropTypes.bool,
  onChange: PropTypes.func.isRequired
};

SocialImage.defaultProps = {
  className: '',
  image: '',
  content: '',
  autoMode: true,
  title: true,
  logo: true,
};

export default SocialImage;
