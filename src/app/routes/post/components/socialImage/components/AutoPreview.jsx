import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/common/Icon/Icon';

const AutoPreview = (props) => {
  const { defaultImage, title, logo, content } = props;

  return (
    <div className={`b-social-image__preview ${!defaultImage ? 'b-social-image__preview_type_cover' : ''}`}>
      {defaultImage ?
        <div>
          <img className="b-social-image__img" src={defaultImage} alt="" />
          {
            (title || logo) &&
            <div className="b-social-image__bar">
              {title && <div className="b-social-image__title">{content}</div>}
              {logo && <Icon name="fireball" className="b-social-image__small-logo" />}
            </div>
          }
        </div>
        :
        <div>
          { logo && <Icon name="fireball-grayish" className="b-social-image__logo" /> }
          { title && <div className="b-social-image__bar"><div className="b-social-image__title">{content}</div></div> }
        </div>
      }
    </div>
  );
};

AutoPreview.propTypes = {
  defaultImage: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  logo: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
};


export default AutoPreview;
