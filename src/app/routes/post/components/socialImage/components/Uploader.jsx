import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/common/Icon/Icon';

const Uploader = (props) => {
  const { image, deleteImage, handleImageUpload } = props;

  let uploaderContent = null;

  if (image) {
    uploaderContent =
      (<div>
        <img className="b-social-image__img" src={image} alt="" />
        <button className="b-social-image__trash" onClick={deleteImage}>
          <Icon name="trash" className="b-social-image__trash-icon" />
        </button>
      </div>);
  } else {
    uploaderContent = (<div>
      <input
        type="file"
        onChange={handleImageUpload}
        className="b-social-image__input"
      />
      <Icon className="b-social-image__upload-icon" name="upload" />
      <div className="b-social-image__upload-text">select an image file to upload or drag it here</div>
    </div>);
  }

  return (
    <div className={`b-social-image__uploader ${!image ? 'b-social-image__uploader_type_no-image' : ''}`}>
      {uploaderContent}
    </div>
  );
};

Uploader.propTypes = {
  image: PropTypes.string.isRequired,
  deleteImage: PropTypes.func.isRequired,
  handleImageUpload: PropTypes.func.isRequired,
};


export default Uploader;
