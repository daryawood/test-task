import React, { Component } from 'react';
import PropTypes from 'prop-types';

class SeoForm extends Component {
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onChange({
      [e.target.name]: e.target.value
    });
  }

  render() {
    const { className, title, description, keywords } = this.props;

    return (
      <div className={`b-seo-form ${className}`}>
        <div className="b-seo-form__row">
          <label htmlFor="metaTitle" className="b-seo-form__label b-small-title">Meta title</label>
          <input
            className="b-seo-form__input"
            id="title"
            name="title"
            type="text"
            onChange={this.handleChange}
            value={title}
            placeholder="Title"
          />
        </div>
        <div className="b-seo-form__row">
          <label htmlFor="metaDescription" className="b-seo-form__label b-small-title">Meta description</label>
          <textarea
            className="b-seo-form__textarea"
            id="description"
            name="description"
            type="text"
            onChange={this.handleChange}
            value={description}
            placeholder="With SEMrush Organic Research data, you will find the best SEO keywords and get a higher ranking."
          />
        </div>
        <div className="b-seo-form__row">
          <label htmlFor="metaKeywords" className="b-seo-form__label b-small-title">Meta keywords</label>
          <textarea
            className="b-seo-form__textarea"
            id="keywords"
            name="keywords"
            type="text"
            onChange={this.handleChange}
            value={keywords}
            placeholder="SEO, SEMrush, organic research"
          />
        </div>
      </div>
    );
  }
}

SeoForm.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  description: PropTypes.string,
  keywords: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

SeoForm.defaultProps = {
  className: '',
  title: '',
  description: '',
  keywords: '',
};

export default SeoForm;
