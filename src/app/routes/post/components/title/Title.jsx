import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/common/Icon/Icon';

class Title extends Component {
  constructor(props) {
    super(props);

    this.state = {
      editMode: false
    };

    this.togglePreview = this.togglePreview.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
  }

  handleChange(e) {
    this.props.onChange(e.target.value);
  }

  togglePreview() {
    this.setState(prevState => ({
      editMode: !prevState.editMode
    }));
  }

  handleKeyDown(e) {
    // enter key
    if (e.keyCode === 13) {
      this.setState({
        editMode: false
      });
    }
  }

  render() {
    const { className, title } = this.props;
    const { editMode } = this.state;

    let titleContent = null;

    if (editMode) {
      titleContent = (<textarea
        value={title}
        className="b-title__textarea"
        type="text"
        onChange={this.handleChange}
        onKeyDown={this.handleKeyDown}
        placeholder="Title"
        autoFocus // eslint-disable-line jsx-a11y/no-autofocus
      />);
    } else {
      titleContent = <h1 className="b-title__heading">{title}</h1>;
    }

    return (
      <div className={`b-title ${className}`}>
        {titleContent}
        <button className="b-title__btn" onClick={this.togglePreview}>
          <Icon
            name="edit"
            className="b-title__icon"
          />
        </button>
      </div>
    );
  }
}

Title.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

Title.defaultProps = {
  className: ''
};

export default Title;
