import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Teaser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      previewMode: false
    };

    this.togglePreview = this.togglePreview.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    this.props.onChange(e.target.value);
  }

  togglePreview() {
    this.setState(prevState => ({
      previewMode: !prevState.previewMode
    }));
  }

  render() {
    const { className, teaser } = this.props;
    const { previewMode } = this.state;

    let title = null;
    let teaserContent = null;

    if (previewMode) {
      title = <div className="b-teaser__title b-small-title">Teaser</div>;
      teaserContent = <div className="b-teaser__preview">{teaser}</div>;
    } else {
      title = <label htmlFor="teaser" className="b-teaser__title b-small-title">Teaser</label>;
      teaserContent = <textarea id="teaser" className="b-teaser__textarea" value={teaser} onChange={this.handleChange} />;
    }

    return (
      <div className={`b-teaser ${className}`}>
        <div className="b-teaser__header">
          {title}
          <button className="b-teaser__btn" onClick={this.togglePreview}>Preview</button>
        </div>
        {teaserContent}
      </div>
    );
  }
}

Teaser.propTypes = {
  className: PropTypes.string,
  teaser: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

Teaser.defaultProps = {
  className: ''
};

export default Teaser;
